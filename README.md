# 🐈 Chat et 🐕 chien Clinique

This project is hosted on [Gitlab Pages](https://robinkite.gitlab.io/c-c-clinic/).<br/>

Welcome to Chat et chien Clinic (Clinic for Cats and Dogs), a group project created by Alex, Tanya, Nastya, and Bohdan. Our goal is to develop an app that helps clinic secretaries manage patient visits with ease.<br/>

Our app currently features two accounts, each with different rights to manage visits. Upon creating a new card, the app validates user inputs and provides helpful feedback if any mistakes are made. "Additionally, if a user enters incorrect values in the input fields, a secret message will appear." <br/>

We hope our app helps streamline clinic operations and makes life a little easier for our furry friends and their owners. <br/>

## 💻 Technologies

The following technologies were used in this project:

- HTML
- SCSS
- JavaScript
- Vite and Prettier

## 🧑‍💼 Project Management

- Trello

## 🐥 Contributors

- Alex Bird 🦅
- Bohdan 😽
- Tanya 🪐
- Nastya 🍀

## 📃 Task Distribution

- Alex Bird:

  - Vite/Gitlab setup;
  - Team management;
  - Code refactor;
  - Bug fixing;
  - Hints;
  - Admin functionality;
  - Cards, Modals;
  - Minor improvements in whole code;
  - Functionality ideas.

- 📦 Bohdan:

  - Sections: header, filters, footer;
  - Sessions, users and authorization;
  - Filters and filtration;
  - Classes: APIHandler, ElementBuilder, Session, Modal, SignInModal, Filter, FiltrableCard;
  - Project structure and refactoring;
  - Project workspace setup (VSCode settings, Prettier config);
  - Quite a bit of love ❤️

- Tanya:

  - Class CardModal;
  - Child class CreateCardModal;
  - Styles: cards, modals (sign-in and create-card), filters form;
  - Design ideas.

- Nastya:

  - Class Visit (cards);
  - Child classes VisitDentist, VisitCardiologist, VisitTherapist;
  - Card buttons functionality.

## 🔑 Accounts

### With admin rights:

login: `admin@clinic.cc`
password: `admin`

### Without admin rights:

login: `guest@clinic.cc`
password: `guest`
