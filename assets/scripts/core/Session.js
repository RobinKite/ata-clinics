import { APIHandler } from "./APIHandler";
import { Workspace } from "./Workspace";
import { findUser } from "../utils";

class SessionStorage {
  static getUser() {
    const email = localStorage.getItem("email");
    const password = localStorage.getItem("password");
    return null || findUser(email, password);
  }

  static setUser(user) {
    localStorage.setItem("email", user.email);
    localStorage.setItem("password", user.password);
  }

  static clear() {
    localStorage.clear();
  }
}

class Session {
  constructor() {
    this.workspace = new Workspace(this);
    this.filters = null;
    this.user = null;
    this.apiHandler = null;
    this.setUserIfStored();
  }

  setUser(user) {
    this.user = user;
    this.apiHandler = new APIHandler(user.apiKey);
    this.workspace.setUp();
    SessionStorage.setUser(user);
  }

  setUserIfStored() {
    const user = SessionStorage.getUser();
    if (user) this.setUser(user);
  }

  removeUser() {
    this.user = null;
    this.apiHandler = null;
    SessionStorage.clear();
  }
}

export const session = new Session();
