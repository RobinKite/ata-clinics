class APIRequestOptions {
  constructor(method, api_key) {
    this.options = {
      method: method,
      headers: {
        Authorization: `Bearer ${api_key}`,
      },
    };
  }

  addHeader(name, value) {
    this.options.headers[name] = value;
    return this;
  }

  setBody(content) {
    this.addHeader("Content-Type", "application/json");
    this.options.body = JSON.stringify(content);
    return this;
  }

  build() {
    return this.options;
  }
}

export class APIHandler {
  BASE_URL = "https://ajax.test-danit.com/api/v2/cards";

  constructor(apiKey) {
    this.apiKey = apiKey;
  }

  async getCards() {
    const options = new APIRequestOptions("GET", this.apiKey).build();
    return await (await this._makeRequest("/", options)).json();
  }

  async postCard(cardData) {
    const options = new APIRequestOptions("POST", this.apiKey)
      .setBody(cardData)
      .build();
    return await this._makeRequest("/", options);
  }

  async editCard(cardData) {
    const options = new APIRequestOptions("PUT", this.apiKey)
      .setBody(cardData)
      .addHeader("Access-Control-Allow-Origin", "*")
      .build();
    return await this._makeRequest(`/${cardData.id}/`, options);
  }

  async deleteCard(identifier) {
    const options = new APIRequestOptions("DELETE", this.apiKey).build();
    return await this._makeRequest(`/${identifier}/`, options);
  }

  async _makeRequest(endpoint, options) {
    return await fetch(this.BASE_URL + endpoint, options);
  }
}
