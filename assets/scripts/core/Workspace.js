import { createVisitCard } from "../components/Visit";
import { Filter } from "../constants/Filter";
import { FiltrableCard } from "../components/FiltrableCard";
import { getSelectOptionValue } from "../utils";
import { toggleHints } from "../handlers/hintsHandler";
import { SignInModal } from "../components/modals/SignInModal";
import { session } from "./Session";
import { ElementBuilder } from "../utils/ElementBuilder";
import { CreateCardModal } from "../components/modals/CreateCardModal";
import { Visit } from "../components/Visit";

const cardActionToHandler = {
  delete: Visit.onDeleteButtonClick,
  edit: Visit.onEditButtonClick,
  showMore: Visit.onShowMoreButtonClick,
};

function HeaderButton(text, value, eventListener) {
  const button = new ElementBuilder("button")
    .addToClassList("header__button")
    .setText(text)
    .setValue(value)
    .build();
  button.addEventListener("click", eventListener);
  return button;
}

export class Workspace {
  constructor(session) {
    this.session = session;
    this.cards = [];
    this.filters = { text: "", urgency: "all", status: "all" };
    this.filtersNode = document.getElementById("filters");
    this.cardsNode = document.getElementById("cards");
    this.headerButtons = document.getElementById("header-buttons");
    this.cardsNode.addEventListener("click", Workspace.onListClickHandler);
    this.createCardModal = new CreateCardModal("create-card");
    this.signInModal = new SignInModal("sign-in-dialog", (user) =>
      this.signInCallback(user)
    );
  }

  static onListClickHandler(event) {
    if (event.target.tagName !== "BUTTON") return;
    const handler = cardActionToHandler[event.target.dataset.action];
    if (handler) handler(event);
  }

  setHeaderStateToNotSignedIn() {
    this.filtersNode.classList.add("hidden");
    this.cardsNode.classList.add("hidden");
    this.headerButtons.append(
      HeaderButton("Sign in", "sign-in", () => this.signInModal.show())
    );
    toggleHints();
    this.headerButtons.querySelector("[value='sign-out']")?.remove();
    this.headerButtons.querySelector("[value='create-card']")?.remove();
  }

  setHeaderStateToSignedIn() {
    this.headerButtons.append(
      HeaderButton("Sign out", "sign-out", () => {
        this.setHeaderStateToNotSignedIn();
        session.removeUser();
      })
    );
    this.headerButtons.append(
      HeaderButton("Create card", "create-card", () => {
        this.createCardModal.openCreateCardModal();
      })
    );
    this.headerButtons.querySelector("[value='sign-in']")?.remove();
    this.cardsNode.classList.remove("hidden");
  }

  signInCallback(user) {
    this.cardsNode.classList.remove("hidden");
    this.cardsNode.innerHTML = "";
    this.setHeaderStateToSignedIn();
    this.session.setUser(user);
  }

  get isUserAdmin() {
    return this.session.user.isAdmin;
  }

  async setUp() {
    this.setUpFilters();
    await this.getCards();
    this.update();
  }

  setUpFilters() {
    const searchField = document.getElementById("search-field-input");
    const visitUrgencyDropdown = document.getElementById("urgency-dropdown");
    const visitStatusDropdown = document.getElementById("status-dropdown");

    searchField.addEventListener("input", (event) =>
      this.changeFilter(Filter.TEXT, event.target.value)
    );
    visitStatusDropdown.addEventListener("change", (event) =>
      this.changeFilter(Filter.STATUS, getSelectOptionValue(event.target))
    );
    visitUrgencyDropdown.addEventListener("change", (event) =>
      this.changeFilter(Filter.URGENCY, getSelectOptionValue(event.target))
    );
  }

  changeFilter(filter, filterValue) {
    this.filters[filter.name] = filterValue;
    this.cards.forEach((card) => card.passThroughFilter(filter));
  }

  update() {
    toggleHints();
    this.filtersNode.classList[!this.cards.length ? "add" : "remove"]("hidden");
  }

  async getCards() {
    const cards = await this.session.apiHandler.getCards();
    if (cards.length === 0) return;
    cards.forEach((data) => {
      if (!data.fullName) return;
      this.createCard(data);
    });
  }

  createCard(data) {
    const callback = async (data) =>
      (await this.session.apiHandler.editCard(data)).ok;
    const visitCard = createVisitCard(data, callback);
    const card = new FiltrableCard(visitCard);
    card.passThroughFilters(Filter.TEXT, Filter.STATUS, Filter.URGENCY);
    card.render(this.cardsNode);
    card.id = data.id;
    this.cards.push(card);
    this.update();
  }

  editCard(id, json) {
    const currentCard = this.cards.find((card) => card.id === id);
    for (const key in json) {
      if (currentCard.card.data[key] === json[key]) continue;
      currentCard.card.data[key] = json[key];
    }
    currentCard.passThroughFilters(Filter.TEXT, Filter.STATUS, Filter.URGENCY);
    return currentCard.card;
  }

  removeCard(id) {
    const index = this.cards.findIndex((card) => card.id === id);
    this.cards.splice(index, 1);
    this.update();
  }
}
