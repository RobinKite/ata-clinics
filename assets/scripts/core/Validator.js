const EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*[A-Za-z0-9]).{4,20}$/;

export class Validator {
  static validateEmail(email) {
    return email.toLowerCase().match(EMAIL_REGEX);
  }

  static validatePassword(password) {
    return password.match(PASSWORD_REGEX);
  }

  static validateCredentials(email, password) {
    return (
      Validator.validateEmail(email) && Validator.validatePassword(password)
    );
  }

  static isAfterToday(dateString) {
    const inputDate = new Date(dateString);
    const today = new Date();
    return inputDate.getTime() > today.getTime();
  }

  static checkPressure(value) {
    return isNaN(parseInt(value)) || value < 50 || value > 160;
  }

  static checkBMI(value) {
    return isNaN(parseInt(value)) || value < 18.5 || value > 30;
  }
}
