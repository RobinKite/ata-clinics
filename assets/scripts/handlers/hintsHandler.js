import { session } from "../core/Session";
import { Modal } from "../components/modals/Modal";

function createToggleHidden(...elements) {
  function wrapper(functionName) {
    elements.forEach((element) => element.classList[functionName]("hidden"));
  }
  return wrapper;
}

export function toggleHints() {
  const hintText = document.querySelector("[data-hint='text']");
  const hintImg = document.querySelector("[data-hint='img']");

  const noCardsText = "Such an empty space... Try adding new cards!";
  const greetingText = "Start by clicking on the 'Sign In' button.";

  setTimeout(async () => {
    hintText.textContent = session.user ? noCardsText : greetingText;
    const toggleHidden = createToggleHidden(hintText, hintImg);

    if (session.user) {
      const cards = await session.apiHandler.getCards();
      toggleHidden(cards.length ? "add" : "remove");
    } else {
      toggleHidden("remove");
    }
  });
}

export function onIncorrectData() {
  const hint = document.querySelector("[data-modal-hint]");
  const toggleHidden = createToggleHidden(hint);
  Modal.state === "error" ? toggleHidden("remove") : toggleHidden("add");
}
