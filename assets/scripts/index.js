import { session } from "./core/Session";

import Sortable from "sortablejs";

document.addEventListener("DOMContentLoaded", () => {
  if (session.user) {
    session.workspace.setHeaderStateToSignedIn();
  } else {
    session.workspace.setHeaderStateToNotSignedIn();
  }

  Sortable.create(document.getElementById("cards"));
});
