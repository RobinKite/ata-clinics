import users from "../../data/users.json";

export function appendToNode(node, ...elements) {
  elements.forEach((element) => node.append(element));
}

export function getSelectOptionValue(selectElement) {
  const optionIndex = selectElement.selectedIndex;
  return selectElement.children[optionIndex].value;
}

export function formatCamelCase(text) {
  const result = text.replace(/([a-z])([A-Z])/g, "$1 $2").toLowerCase();
  return result.charAt(0).toUpperCase() + result.slice(1);
}

export async function SHA256(string) {
  const utf8 = new TextEncoder().encode(string);
  const hashBuffer = await crypto.subtle.digest("SHA-256", utf8);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray
    .map((bytes) => bytes.toString(16).padStart(2, "0"))
    .join("");
  return hashHex;
}

export function findUser(email, password) {
  for (const user of users) {
    const isValidUser = email === user.email && password === user.password;
    if (isValidUser) return user;
  }
  return null;
}
