export class ElementBuilder {
  constructor(tagName) {
    this.element = document.createElement(tagName);
  }

  addToClassList(...classNames) {
    for (let className of classNames) {
      if (className) {
        this.element.classList.add(className);
      }
    }
    return this;
  }

  setId(id) {
    this.element.id = id;
    return this;
  }

  setText(text) {
    this.element.textContent = text;
    return this;
  }

  setType(type) {
    this.element.type = type;
    return this;
  }

  setValue(value) {
    this.element.value = value;
    return this;
  }

  addDataset(attr, value = null) {
    this.element.dataset[attr] = value;
    return this;
  }

  addAttr(attr, value = null) {
    if (!attr) return this;
    this.element.setAttribute(attr, value);
    return this;
  }

  toggleReadonly() {
    this.element.readOnly = !this.element.readOnly ?? true;
    return this;
  }

  build() {
    return this.element;
  }
}
