import { onIncorrectData } from "../../handlers/hintsHandler";

class ModalState {
  static CLOSED = "closed";
  static OPENED = "opened";
}

export class Modal {
  static _state = ModalState.CLOSED;

  constructor(dialogNodeId) {
    this.node = document.getElementById(dialogNodeId);
    this.dismissOnBackdropClick = this.dismissOnBackdropClick.bind(this);
    this.close = this.close.bind(this);
  }

  static set state(newState) {
    const oldState = Modal._state;
    Modal._state = newState;
    if (newState !== oldState) {
      onIncorrectData();
    }
  }

  static get state() {
    return Modal._state;
  }

  dismissOnBackdropClick(event) {
    // NOTE: Fixes modal closure when select is clicked in Firefox 🤷‍♂️
    if ((!event.clientY, !event.clientX)) return;
    const box = this.node.getBoundingClientRect();
    const isBackdrop =
      event.clientY < box.top ||
      event.clientY > box.top + box.height ||
      event.clientX < box.left ||
      event.clientX > box.left + box.width;
    if (isBackdrop) this.close();
  }

  show() {
    this.node.showModal();
    this.node.addEventListener("mousedown", this.dismissOnBackdropClick);
    Modal.state = ModalState.OPENED;
  }

  close() {
    this.node.close();
    this.node.removeEventListener("mousedown", this.dismissOnBackdropClick);
    Modal.state = ModalState.CLOSED;
  }

  select(selector) {
    return this.node.querySelector(selector);
  }
}
