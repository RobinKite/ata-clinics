import { session } from "../../core/Session";
import { CardModal } from "./CardModal";
import { Modal } from "./Modal";

export class CreateCardModal extends CardModal {
  constructor(nodeId, method = "create") {
    super(nodeId, method);
    this.openCreateCardModal = this.openCreateCardModal.bind(this);
    this.addClassToDateLabel = this.addClassToDateLabel.bind(this);
    this.onModalSubmit = this.onModalSubmit.bind(this);
    this.active()
  }

  openCreateCardModal() {
    super.show();
    this.changeTextContent("Create Your Appointment", "Create visit");
    this.form.addEventListener("submit", this.onModalSubmit);
  }

  close() {
    super.close();
    this.form.removeEventListener("submit", this.onModalSubmit);
  }

  async onModalSubmit(event) {
    event.preventDefault();

    if (Modal.state === "error") return;

    const formData = new FormData(event.currentTarget);
    const data = Object.fromEntries(formData);
    data.status = "open";
    const response = await session.apiHandler.postCard(data);
    const json = await response.json();
    data.id = json.id;
    if (response.ok) session.workspace.createCard(data);
    event.target.reset();
    this.close();
  }
}
