import { Validator } from "../../core/Validator";
import { Modal } from "./Modal";
import { session } from "../../core/Session";

export class CardModal extends Modal {
  constructor(nodeId, method) {
    super(nodeId);
    this.method = method;
    this.form = this.select("#clinic-form");
    this.title = this.select(".appointment-legend");
    this.submitBtn = this.select(".create-visit");
    this.closeBtn = this.select(".close-btn");
    this.pressure = this.select("#pressure-input");
    this.bodyIndex = this.select("#body-index-input");
    this.cardio = this.select("#cardio-input");
    this.dateOfBirth = this.select("#birth-date-input");
    this.lastVisit = this.select("#last-visit-input");
    this.doctorsSelect = this.select("#doctors-select");
    this.urgencySelect = this.select("#urgency-select");
    this.doctorsMap = {
      cardiologist: [
        this.pressure,
        this.bodyIndex,
        this.cardio,
        this.dateOfBirth,
      ],
      dentist: [this.lastVisit],
      therapist: [this.dateOfBirth],
    };
  }

  changeTextContent(title, btn) {
    this.title.textContent = title;
    this.submitBtn.textContent = btn;
  }

  active() {
    this.addClassToDateLabel();

    this.pressure.addEventListener(
      "change",
      createInputEventListener(
        Validator.checkPressure,
        "Possible values: only numbers from 50 to 160."
      )
    );

    this.bodyIndex.addEventListener(
      "change",
      createInputEventListener(
        Validator.checkBMI,
        "Possible values: only numbers from 18.5 to 30."
      )
    );

    this.dateOfBirth.addEventListener(
      "change",
      createInputEventListener(
        Validator.isAfterToday,
        "Possible values: date only before today.",
        true
      )
    );

    this.lastVisit.addEventListener(
      "change",
      createInputEventListener(
        Validator.isAfterToday,
        "Possible values: date only before today.",
        true
      )
    );

    this.doctorsSelect.addEventListener("change", (event) => {
      this.showDoctorsFields(event.target.value);
    });

    this.closeBtn.addEventListener("click", this.close);

    function createInputEventListener(validator, errorMessage, isDate = null) {
      return function (event) {
        if (isDate)
          event.target.classList[event.target.value ? "add" : "remove"](
            "appointment__input--active"
          );

        if (event.target.value && validator(event.target.value)) {
          Modal.state = "error";
          event.target.classList.add("incorrect-value");
          event.target.title = errorMessage;
        } else {
          Modal.state = "opened";
          event.target.classList.remove("incorrect-value");
          delete event.target.title;
        }
      };
    }
  }

  showDoctorsFields(inputValue) {
    if (!inputValue) return;

    Modal.state = "opened";
    this.clearLabelsActive(true);
    this.doctorsMap[inputValue].forEach((input) => {
      const parent = input.closest(".appointment");
      input.disabled = false;
      parent.classList.remove("appointment--hidden");
    });
  }

  clearLabelsActive(onlyActive) {
    const className = "appointment__label--active";
    const activeLabels = this.form.querySelectorAll(`.${className}`);
    const defaultHiddenInputs = [
      this.pressure,
      this.bodyIndex,
      this.cardio,
      this.dateOfBirth,
      this.lastVisit,
    ];

    if (!onlyActive) {
      this.urgencySelect.selectedIndex = 0;
      this.doctorsSelect.selectedIndex = 0;
      activeLabels.forEach((label) => {
        const input = this.select(`#${label.htmlFor}`);
        input.value = "";
        input.classList.remove("appointment__input--active", "incorrect-value");
        label.classList.remove(className);
      });
    }

    defaultHiddenInputs.forEach((input) => {
      const parent = input.closest(".appointment");
      parent.classList.add("appointment--hidden");
      input.disabled = true;
    });
  }

  addClassToDateLabel() {
    const inputLabelMap = this.getInputLabel();

    inputLabelMap.forEach((value, key, map) => {
      key.addEventListener("change", () => {
        this.inputHandler(key, value);
      });
    });
  }

  getInputLabel() {
    const inputsDate = this.form.querySelectorAll("input");
    const inputsMap = new Map();

    inputsDate.forEach((input) => {
      const label = input.nextElementSibling;
      inputsMap.set(input, label);
    });
    return inputsMap;
  }

  inputHandler(input, label) {
    label.classList[input.value ? "add" : "remove"](
      "appointment__label--active"
    );
  }

  show() {
    if (!session.workspace.isUserAdmin) {
      alert(`Only admin can ${this.method} cards! 😿`);
      return;
    }
    super.show();
  }

  close() {
    super.close();
    this.clearLabelsActive();
  }
}
