import { Modal } from "./Modal";
import { Validator } from "../../core/Validator";
import { findUser, SHA256 } from "../../utils";

export class SignInModal extends Modal {
  constructor(dialogNodeId, signInCallback) {
    super(dialogNodeId);
    this.signInCallback = signInCallback;
    this.emailInput = this.select("[name='email']");
    this.passwordInput = this.select("[name='password']");
    this.closeButton = this.select("[type='button']");
    this.submitButton = this.select("[type='submit']");
    this.submit = this.submit.bind(this);
  }

  show() {
    super.show();
    this.submitButton.addEventListener("click", this.submit);
    this.closeButton.addEventListener("click", this.close);
  }

  close() {
    super.close();
    this.submitButton.removeEventListener("click", this.submit);
    this.closeButton.removeEventListener("click", this.close);
  }

  async submit(event) {
    event.preventDefault();

    const email = this.emailInput.value;
    const password = this.passwordInput.value;
    if (!Validator.validateCredentials(email, password))
      return this.handleInvalidCredentials();

    const user = findUser(email, await SHA256(password));
    if (!user) return this.handleInvalidCredentials();
    this.signInCallback(user);
    this.close();
  }

  handleInvalidCredentials() {
    console.error("Invalid credentials");
  }
}
