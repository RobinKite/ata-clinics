import { session } from "../../core/Session";
import { CardModal } from "./CardModal";
import { Modal } from "./Modal";

export class EditCardModal extends CardModal {
  constructor(nodeId, method = "edit") {
    super(nodeId, method);
    this.method = method;
    this.onModalSubmit = this.onModalSubmit.bind(this);
  }

  async onModalSubmit(event) {
    event.preventDefault();

    if (Modal.state === "error") return;

    const formData = new FormData(event.currentTarget);
    const data = Object.fromEntries(formData);
    data.id = this.id;
    const response = await session.apiHandler.editCard(data);
    const json = await response.json();
    if (response.ok) {
      const li = await session.workspace.editCard(this.id, json).node;
      const inputs = li.querySelectorAll(".cards__input");

      inputs.forEach((input) => {
        const propName = input.name;
        if (data[propName]) {
          input.value = data[propName];
        }
      });
    }
    event.target.reset();
    this.close();
  }

  openCreateCardModal(id) {
    if (!id) return;
    super.show();
    this.id = parseInt(id);
    this.autoEnterFields(this.id);
    this.changeTextContent("Edit Your Appointment", "Save");
    this.form.addEventListener("submit", this.onModalSubmit);
  }

  close() {
    super.close();
    this.form.removeEventListener("submit", this.onModalSubmit);
  }

  autoEnterFields(id) {
    const modalInputs = this.node.querySelectorAll(".appointment__input");

    const card = document.getElementById(id);
    const cardInputs = card.querySelectorAll(".cards__input");
    const cardMap = {};
    cardInputs.forEach((input) => {
      cardMap[input.name] = input;
    });
    const names = Object.keys(cardMap);

    const doctorOptions = Array.from(
      this.doctorsSelect.querySelectorAll("option")
    ).map((option) => option.value);
    const doctorIndex = doctorOptions.findIndex(
      (item) => cardMap["doctor"].value === item
    );
    this.doctorsSelect.selectedIndex = doctorIndex;

    const urgencyOptions = Array.from(
      this.urgencySelect.querySelectorAll("option")
    ).map((option) => option.value);
    const urgencyIndex = urgencyOptions.findIndex(
      (item) => cardMap["urgency"].value === item
    );
    this.urgencySelect.selectedIndex = urgencyIndex;

    this.showDoctorsFields(cardMap["doctor"].value);

    modalInputs.forEach((input) => {
      if (
        input.name !== "doctor" &&
        input.name !== "urgency" &&
        names.includes(input.name)
      ) {
        const label = input.nextElementSibling;
        label.classList.add("appointment__label--active");
        input.value = cardMap[input.name].value;
        if (input.type === "date") {
          input.classList.add("appointment__input--active");
        }
      }
    });
  }
}
