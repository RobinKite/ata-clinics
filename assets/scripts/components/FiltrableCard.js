export class FiltrableCard {
  constructor(card) {
    this.card = card;
    this.render = this.card.render.bind(this.card);
    this.passedFilters = {};
  }

  passThroughFilter(filter) {
    this.passedFilters[filter.name] = filter.func(this.card.data);
    this.areFiltersPassed ? this.show() : this.hide();
  }

  passThroughFilters(...filters) {
    for (const filter of filters) {
      this.passThroughFilter(filter);
    }
  }

  get areFiltersPassed() {
    return Object.values(this.passedFilters).every(Boolean);
  }

  show() {
    this.card.node.classList.remove("hidden");
  }

  hide() {
    this.card.node.classList.add("hidden");
  }
}
