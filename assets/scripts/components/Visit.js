import { ElementBuilder } from "../utils/ElementBuilder";
import { appendToNode, formatCamelCase, getSelectOptionValue } from "../utils";
import { Filter } from "../constants/Filter";
import { session } from "../core/Session";
import { EditCardModal } from "./modals/EditCardModal";

class VisitBricks {
  static Container = () =>
    new ElementBuilder("div").addToClassList("cards__buttons").build();
  static Item = (id) =>
    new ElementBuilder("li").addToClassList("cards__item").setId(id).build();
  static Checkbox = (name, value, id) =>
    new ElementBuilder("input")
      .addToClassList("cards__status-checkbox", "checkbox__input")
      .setType("checkbox")
      .addAttr("name", `${name}-${id}`)
      .setId(`${name}-${id}`)
      .setValue(value)
      .build();
  static Input = (name, value) =>
    new ElementBuilder("input")
      .addToClassList("cards__input")
      .setValue(value)
      .addAttr("name", name)
      .toggleReadonly()
      .build();
  static Label = (text, labelId, isHidden, checkboxId) =>
    new ElementBuilder("label")
      .addToClassList(
        "cards__label",
        isHidden ? "cards__label--hidden" : null,
        checkboxId ? "checkbox" : null
      )
      .addAttr(
        "title",
        checkboxId
          ? "Current status."
          : "Click the edit button below to modify your input."
      )
      .addAttr("for", checkboxId ? `${labelId}-${checkboxId}` : `${labelId}`)
      .setText(formatCamelCase(text))
      .build();
  static Button = (text, datasetValue) =>
    new ElementBuilder("button")
      .addToClassList("cards__btn")
      .setType("button")
      .setText(text)
      .addDataset("action", datasetValue)
      .build();
}

class BrickSelector {
  static INPUT = ".cards__input";
  static CHECKBOX = ".cards__status-checkbox";
}

export class Visit {
  constructor(data, updateDataCallaback) {
    this.node = VisitBricks.Item(data.id);
    this.data = data;
    this.updateDataCallaback = updateDataCallaback;
  }

  async update() {
    const isSuccessful = await this.updateDataCallaback(this.data);
    if (!isSuccessful) return;

    const inputElements = this.node.querySelectorAll(BrickSelector.INPUT);
    for (const inputElement of inputElements) {
      inputElement.value = this.data[inputElement.name];
    }
    const checkboxElement = this.node.querySelector(BrickSelector.CHECKBOX);
    checkboxElement.value = this.data.status;
    checkboxElement.checked = this.data.status === "done";
  }

  render(parentNode) {
    const defaultFields = ["doctor", "description", "fullName", "urgency"];
    const excludedFields = ["id", "status"];

    const form = new ElementBuilder("form")
      .addToClassList("cards__form")
      .build();

    const cardTitle = new ElementBuilder("h2")
      .addToClassList("cards__title")
      .setText(`Card #${this.data.id}`)
      .build();

    form.append(cardTitle);

    Object.keys(this.data).forEach((field) => {
      const fieldValue = this.data[field];
      if (excludedFields.includes(field) || !fieldValue) return;

      const isHidden = !defaultFields.includes(field);
      const label = VisitBricks.Label(field, field, isHidden);
      const input = VisitBricks.Input(field, fieldValue);
      label.append(input);
      form.append(label);
    });

    const buttonContainer = VisitBricks.Container();
    const showMoreButton = VisitBricks.Button("Show more", "showMore");
    const editButton = VisitBricks.Button("Edit", "edit");
    const deleteButton = VisitBricks.Button("Delete", "delete");
    appendToNode(buttonContainer, showMoreButton, editButton, deleteButton);

    const statusLabel = VisitBricks.Label(
      "",
      "status-checkbox",
      null,
      this.data.id
    );
    const statusCheckbox = VisitBricks.Checkbox(
      "status-checkbox",
      this.data.status,
      this.data.id
    );
    if (this.data.status === "done") statusCheckbox.checked = true;
    statusCheckbox.addEventListener("click", (e) => this.updateStatus(e));

    const statusSpan = new ElementBuilder("span")
      .addToClassList("checkbox__span")
      .build();

    appendToNode(statusLabel, statusCheckbox, statusSpan);
    appendToNode(form, statusLabel, buttonContainer);
    appendToNode(this.node, form);
    parentNode.prepend(this.node);
  }

  async updateStatus(event) {
    this.data.status = this.data.status === "open" ? "done" : "open";
    event.target.value = this.data.status;
    await this.update();

    const currentStatus = document.getElementById("status-dropdown");
    session.workspace.changeFilter(
      Filter.STATUS,
      getSelectOptionValue(currentStatus)
    );
  }

  static onShowMoreButtonClick(event) {
    const button = event.target;
    const itemNode = button.closest(".cards__item");

    if (button.textContent === "Show more") {
      const array = itemNode.querySelectorAll(".cards__label--hidden");
      array.forEach((label) => {
        label.classList.remove("cards__label--hidden");
        label.dataset.isShown = null;
      });
      button.dataset.text = button.textContent;
      button.textContent = "Show less";
    } else {
      const array = itemNode.querySelectorAll("[data-is-shown]");
      array.forEach((label) => {
        label.classList.add("cards__label--hidden");
        delete label.dataset.isShown;
      });

      button.textContent = button.dataset.text;
      delete button.dataset.text;
    }
  }

  static async onDeleteButtonClick(event) {
    if (!session.workspace.isUserAdmin) {
      return alert("Only admin can delete cards! 😿");
    }
    const itemNode = event.target.closest(".cards__item");
    const response = await session.apiHandler.deleteCard(itemNode.id);
    if (response.ok) {
      itemNode.remove();
      session.workspace.removeCard(itemNode.id);
    }
  }

  static onEditButtonClick(event) {
    const editCardModal = new EditCardModal("create-card");
    const itemNode = event.target.closest(".cards__item");
    editCardModal.openCreateCardModal(itemNode.id);
  }
}

export class VisitDentist extends Visit {
  constructor(...args) {
    super(...args);
    this.last_visit = this.data.last_visit;
  }
}

export class VisitCardiologist extends Visit {
  constructor(...args) {
    super(...args);
    this.weight = this.data.weight;
    this.br = this.data.br;
    this.age = this.data.age;
  }
}

export class VisitTherapist extends Visit {
  constructor(...args) {
    super(...args);
    this.age = this.data.age;
  }
}

const doctorToVisitClassMap = new Map([
  ["dentist", VisitDentist],
  ["cardiologist", VisitCardiologist],
  ["therapist", VisitTherapist],
]);

export function createVisitCard(data, dataUpdateCallback) {
  const mappedClass = doctorToVisitClassMap.get(data.doctor);
  if (mappedClass) return new mappedClass(data, dataUpdateCallback);
  return null;
}
