import { session } from "../core/Session";

export class Filter {
  static TEXT = {
    name: "text",
    func: (object) => Filter.hasText(object, Filter.getValue("text")),
  };
  static URGENCY = {
    name: "urgency",
    func: (object) =>
      Filter.hasValue(object.urgency, Filter.getValue("urgency")),
  };
  static STATUS = {
    name: "status",
    func: (object) => Filter.hasValue(object.status, Filter.getValue("status")),
  };

  static getValue(filterName) {
    return session.workspace.filters[filterName];
  }

  static hasValue(value, expectedValue) {
    return expectedValue === "all" || value === expectedValue;
  }

  static hasText(object, textToFind) {
    const toLowercasedText = (array) => array.join("").toLowerCase();
    const text = toLowercasedText(Object.values(object));
    return text.search(textToFind.toLowerCase()) !== -1;
  }
}
